﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_Solo_Task
{
    class pizza
    {
        public string name = "Pizza";
        public string description = "description written individually for each pizza";
        public string sprice = "7.50"; //default price
        public string mprice = "13.50"; //default price
        public string lprice = "19.00"; //default price
    }

    class client
    {
        public string username = "First name";
        public string useraddress = "address written seperately";
        public string usernote = "note written seperately";
    }


    class Program

    {

        public static void Main(string[] args)
        {
            //Welcome Screen
            Console.WriteLine("            __");
            Console.WriteLine("          // --.._            Welcome to");
            Console.WriteLine("         ||  (_)  _  -._      the pizza ordering machine 7000(tm) ");
            Console.WriteLine("         ||    _ (_)    '-.");
            Console.WriteLine("         ||   (_)   __..-'    Please press enter");
            Console.WriteLine("          \\__..--            To continue!");

            Console.ReadLine();
            Console.Clear();

            //Select Pizza
            Console.WriteLine("===================================================");
            Console.WriteLine("===============Our Pizza Selection=================");
            Console.WriteLine("===================================================");
            Console.WriteLine("==================1. Pepperoni=====================");
            Console.WriteLine("=============2. Chicken and Cranberry==============");
            Console.WriteLine("==============3. Salmon and Seafood================");
            Console.WriteLine("===================================================");
            Console.WriteLine(""); //break
            Console.WriteLine("===Please select the corresponding number to the===");
            Console.WriteLine("===========pizza you would like to order===========");

            var choosepizza = Console.ReadLine();
            Console.Clear();

            switch (choosepizza)
            {
                case "1":
                    orderpepperoni();
                    break;
                case "2":
                    orderchiccran();
                    break;
                case "3":
                    ordersalmon();
                    break;
            }
            //Order Drinks
            Console.WriteLine("Would you like to order a drink to accompany your pizza?");
            Console.WriteLine("Press 1 to order a drink or 0 to skip.");

            var buydrinks = Console.ReadLine();
            Console.Clear();

            switch (buydrinks)
            {
                case "1":
                    orderdrinks();
                    break;
                case "0":
                    break;

            }

            Console.WriteLine(""); //break
            Console.WriteLine("Press enter to progress your order");
            Console.ReadLine();
            Console.Clear();

            userdetails();




            Console.WriteLine(""); //break
            Console.WriteLine("Press enter to progress your order");
            Console.ReadLine();
            Console.Clear();

            userpayment();

            Console.WriteLine(""); //break
            Console.WriteLine("Thankyou for using the Pizza Ordering Machine 7000 (tm)");
            Console.WriteLine("Your order will be delivered soon, please look forward to it");
            Console.WriteLine(""); //break
            Console.WriteLine("Press any key to exit");
            Console.ReadKey();

        }



        public static void orderpepperoni()
        {
            pizza Pepperoni = new pizza();
            Pepperoni.name = "Pepperoni";
            Console.WriteLine(""); //break
            Pepperoni.description = "A devilishly spicy Pepperoni pizza with an assortment of luxury cured meats, red onion and anchovy. Served on a traditional tomato base and topped with mozzerella";
            //has default prices so don't need s/m/lprices 
            Console.WriteLine("Pepperoni Pizza");
            Console.WriteLine(""); //break
            Console.WriteLine($"{Pepperoni.description}");
            Console.WriteLine(""); //break
            Console.WriteLine("Press enter to order");
            Console.ReadLine();
            Console.WriteLine(""); //break
            Console.WriteLine("Please select what size of pizza you would like using the corresponding number");
            Console.WriteLine(""); //break
            Console.WriteLine($"1: Small 9 inch Pizza Price: ${Pepperoni.sprice}");
            Console.WriteLine($"2: Medium 12 inch Pizza Price: ${Pepperoni.mprice}");
            Console.WriteLine($"3: Large 14 inch Pizza Price: ${Pepperoni.lprice}");

            var pepperonichoices = new Dictionary<string, string>();
            

            var choosepepperoniprice = Console.ReadLine();

            switch (choosepepperoniprice)
            {
                case "1":
                    pepperonichoices.Add($"{Pepperoni}", $"{Pepperoni.sprice}");
                    Console.WriteLine($"Order for Small Pepperoni pizza for ${Pepperoni.sprice} placed");
                    break;
                case "2":
                    pepperonichoices.Add($"{Pepperoni}", $"{Pepperoni.mprice}");
                    Console.WriteLine($"Order for Medium Pepperoni pizza for ${Pepperoni.mprice} placed");
                    break;
                case "3":
                    pepperonichoices.Add($"{Pepperoni}", $"{Pepperoni.lprice}");
                    Console.WriteLine($"Order for Large Pepperoni pizza for ${Pepperoni.lprice} placed");
                    break;
            }

            Console.WriteLine(""); //break
            Console.WriteLine("Please press enter to progress your order");
            Console.ReadLine();

        }

        public static void orderchiccran()
        {
            pizza chiccran = new pizza();
            chiccran.name = "Chicken and Cranberry";
            Console.WriteLine(""); //break
            chiccran.description = "A Tasty Chicken and Cranberry pizza with camembert, red onion and capers for a godly combo of tasty, creamy and sweet. Served on a traditional tomato base and topped with mozzerella";
            //has default prices so don't need s/m/lprices 
            Console.WriteLine("Chicken and Cranberry Pizza");
            Console.WriteLine(""); //break
            Console.WriteLine($"{chiccran.description}");
            Console.WriteLine(""); //break
            Console.WriteLine("Press enter to order");
            Console.ReadLine();
            Console.WriteLine(""); //break
            Console.WriteLine("Please select what size of pizza you would like using the corresponding number");
            Console.WriteLine(""); //break
            Console.WriteLine($"1: Small 9 inch Pizza Price: ${chiccran.sprice}");
            Console.WriteLine($"2: Medium 12 inch Pizza Price: ${chiccran.mprice}");
            Console.WriteLine($"3: Large 14 inch Pizza Price: ${chiccran.lprice}");

            var chiccranchoices = new Dictionary<string, string>();

            var choosechiccranprice = Console.ReadLine();

            switch (choosechiccranprice)
            {
                case "1":
                    chiccranchoices.Add($"{chiccran}", $"{chiccran.sprice}");
                    Console.WriteLine($"Order for Small Chicken and Cranberry pizza for ${chiccran.sprice} placed");
                    break;
                case "2":
                    chiccranchoices.Add($"{chiccran}", $"{chiccran.mprice}");
                    Console.WriteLine($"Order for Medium Chicken and Cranberry pizza for ${chiccran.mprice} placed");
                    break;
                case "3":
                    chiccranchoices.Add($"{chiccran}", $"{chiccran.lprice}");
                    Console.WriteLine($"Order for Large Chicken and Cranberry pizza for ${chiccran.lprice} placed");
                    break;
            }

            Console.WriteLine(""); //break
            Console.WriteLine("Press enter to progress your order");
            Console.ReadLine();

        }
        public static void ordersalmon()
        {
            pizza Salmon = new pizza();
            Salmon.name = "Salmon and Seafood";
            Salmon.description = "Our award winning premium Salmon and seafood Pizza! This pizza has succulent chunks of prime salmon, garlic prawns, mussels and scallops all served along with a sprinkling of crumbly feta. Served on a modified creamy tomato base and topped with mozzerela. ";
            Salmon.sprice = "10.50";
            Salmon.mprice = "16.50";
            Salmon.lprice = "22.00";
            //has default prices so don't need s/m/lprices 
            Console.WriteLine(""); //break
            Console.WriteLine("Salmon and Seafood Pizza");
            Console.WriteLine(""); //break
            Console.WriteLine($"{Salmon.description}");
            Console.WriteLine(""); //break
            Console.WriteLine("Press enter to order");
            Console.ReadLine();
            Console.WriteLine(""); //break
            Console.WriteLine("Please select what size of pizza you would like using the corresponding number");
            Console.WriteLine(""); //break
            Console.WriteLine($"1: Small 9 inch Pizza Price: ${Salmon.sprice}");
            Console.WriteLine($"2: Medium 12 inch Pizza Price: ${Salmon.mprice}");
            Console.WriteLine($"3: Large 14 inch Pizza Price: ${Salmon.lprice}");

            var Salmonchoices = new Dictionary<string, string>();

            var choosesalmonprice = Console.ReadLine();

            switch (choosesalmonprice)
            {
                case "1":
                    Salmonchoices.Add($"{Salmon}", $"{Salmon.sprice}");
                    Console.WriteLine($"Order for Small Salmon and Seafood pizza for ${Salmon.sprice} placed");
                    break;
                case "2":
                    Salmonchoices.Add($"{Salmon}", $"{Salmon.mprice}");
                    Console.WriteLine($"Order for Medium Salmon and Seafood pizza for ${Salmon.mprice} placed");
                    break;
                case "3":
                    Salmonchoices.Add($"{Salmon}", $"{Salmon.lprice}");
                    Console.WriteLine($"Order for Large Salmon and Seafood pizza for ${Salmon.lprice} placed");
                    break;
            }

            Console.WriteLine(""); //break
            Console.WriteLine("Press enter to progress your order");
            Console.ReadLine();

        }
        public static void orderdrinks()
        {
            Console.WriteLine("-------Our drink selection:--------");
            Console.WriteLine("------------- 1: Coke -------------");
            Console.WriteLine("------------- 2: Pepsi ------------");
            Console.WriteLine("------------- 3: Sprite -----------");
            Console.WriteLine("--------All drinks are $3.50-------");
            Console.WriteLine("-----------------------------------");
            Console.WriteLine("-Please enter corresponding number-");
            Console.WriteLine("----------to select drink----------");

            List<string> drinkchoice = new List<string>();

            var choosedrinkchoice = Console.ReadLine();


            switch (choosedrinkchoice)
            {
                case "1": //Coke
                    Console.WriteLine($"Order for Coke placed for $3.50");
                    drinkchoice.Add($"Coke");
                    break;
                case "2": //Pepsi
                    Console.WriteLine($"Order for Pepsi placed for $3.50");
                    drinkchoice.Add($"Pepsi");
                    break;
                case "3": //Sprite
                    Console.WriteLine($"Order for Sprite placed for $3.50");
                    drinkchoice.Add($"Sprite");
                    break;
            }

        }

        public static void userdetails()
        {
            Console.WriteLine("Thankyou for your order, please fill out the following information so we can deliver it");
            Console.WriteLine(""); //break
            client currentuser = new client();

            Console.WriteLine("Please enter your first name");
            currentuser.username = Console.ReadLine();
            Console.WriteLine(""); //break

            Console.WriteLine("Please enter your address (Formatted as house Number, Street name, suburb)");
            currentuser.useraddress = Console.ReadLine();
            Console.WriteLine(""); //break

            Console.WriteLine("Please enter any additional information we should know, such as if you want an ingredient left off your pizza, or you have a dog or gate at your house");
            currentuser.usernote = Console.ReadLine();
            Console.WriteLine(""); //break

        }

        public static void userpayment()
        {

            Console.WriteLine("Payment:");
            Console.WriteLine("What payment method would you like to use?");
            Console.WriteLine("Press 1 for pay now credit card, 2 for cash or 3 for credit card pay on delivery");

            var choosepayment = Console.ReadLine();

            switch (choosepayment)
            {
                case "1":
                    Console.WriteLine("Credit Card Entry:");
                    Console.WriteLine(""); //break
                    Console.WriteLine("Please enter your credit card number (formatted as 1234-5678-9123-4567 or 1234567891234567)");
                    Console.ReadLine();
                    Console.WriteLine(""); //break
                    Console.WriteLine("Please enter your security code, you can find this on the back of your card (formatted as 123)");
                    Console.ReadLine();
                    Console.WriteLine("Credit card details confirmed");
                    break;
                case "2":
                    Console.WriteLine("You will be asked for your payment on the delivery of your order");
                    break;
                case "3":
                    Console.WriteLine("You will be asked for your payment on the delivery of your order");
                    break;
            }

        }

    }
}






